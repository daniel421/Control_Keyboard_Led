//V2.2
//This program make the scroll
#include <time.h>
#include <stdio.h>
#include <linux/input.h>

void writeDiode(FILE *f, int code, int valeur){
    //  Fonction to send data to the KeyBoard
    //  Modif struct e
    //  send 0 or 1 via @valeur
struct input_event e;
e.type= EV_LED;

        switch(code){
                case 0:
                        e.code=LED_NUML;
                case 1:
                        e.code=LED_CAPSL;
                case 2:
                        e.code=LED_SCROLLL;
                }
                //Debug purpose printf("Received code: %d\n", code );

        e.value=valeur;
        fwrite(&e, sizeof(e), 1, f);
        fflush(f);
}

int main(int argc, char *argv[])
{
    FILE *file;
    file = fopen(argv[1], "w"); //Open the file
    struct timespec periode;
    periode.tv_sec=0;
    periode.tv_nsec=500*1000000L;//Wait 500 milisec

//For the beginning all led must be OFF
    writeDiode(file, 1, 0);
    writeDiode(file, 2, 0);
    writeDiode(file, 3, 0);
//Now just need to do this for eternity
    while(1==1){
        int i=0;
            for(i=0; i<=2;i++){
                writeDiode(file, i, 1);    //Turn on one led
                //Debug printf("led %d is on\n", i );
                nanosleep(&periode, NULL); //Wait the delay
                //Debug printf("waiting\n", i );
                writeDiode(file, i, 0);     //Turn of this led
                //Debug printf("led %d is off\n", i );
                nanosleep(&periode, NULL); //Wait the delay in "OFF" mode
                //Debug printf("waiting\n\n", i );
            }
        }
}
